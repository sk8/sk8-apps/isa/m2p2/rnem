- original code by Florence Ghestem (fall 2020) based on ideas and previous code by Suzanne Touzeau, Vincent Calcagno and Ludovic Mailleret

- some small modifications by Ludovic Mailleret (spring 2021)

- more important modifications by Ludovic Mailleret and Suzanne Touzeau (Q3, Q4 2022)

- SK8 port, code formatting by Ludovic Mailleret (summer 2024)

project supported by the Interlude project funded by OFB as part of the Ecophyto call on “ Territorial levers to reduce the use and risks linked to phytopharmaceutical products” launched by the French Ministries in charge of Ecology, Agriculture, Health and Research.
