# LaTeX formulas for Rnem interface

## modèle matière organique

$$
\left\{
\begin{array}{l}
\dot P_a = -\beta P_a H^S - \eta P_a + r I_a,\\
\dot H^S = \mu x\, f(H^S, E_a, I_a) - \varepsilon_a^S \beta P_a H^S,\\
\dot E_a = \varepsilon_a^S \beta P_a H^S - \lambda E_a,\\
\dot I_a = \lambda E_a - \alpha I_a.
\end{array}
\right.
$$





## modèle rotations de résistances

$$
\left\{
\begin{array}{l}
\dot P_a = -\beta P_a H^X - \eta P_a + (1-\delta)r I_a,\\
\dot P_v = -\beta P_v H^X - \eta P_v + \delta r I_a + (1-w_r)r I_v,\\
\dot H^X = \mu x\, f(H^X, E_a+E_v, I_a+I_v) - 
    \varepsilon_a^X \beta P_a H^X -(1-w_\beta)\varepsilon_v^X \beta P_v H^X,\\
\dot E_a = \varepsilon_a^X \beta P_a H^X - \lambda E_a,\\
\dot E_v = (1-w_\beta)\varepsilon_v^X \beta P_v H^X - \lambda E_v,\\
\dot I_a = \lambda E_a - \alpha I_a,\\
\dot I_v = \lambda E_v - \alpha I_v.
\end{array}
\right.
$$


