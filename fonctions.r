# Fonctions utilisées 

#fonction qui retourne le vecteur des valeurs de Epsilon_a en fonction des rotations
rotation_app<-function(n,P1,P2){
  a<-rep(0,P1) #Pattern plante 1
  b<-rep(1,P2) #pattern plante 2
  c<-rep(c(a,b),length.out = n) #repetition jusque n
  
  return(c)
}

# definition du modele pour l'onglet MO
# definition du modele
une_saison_MO<-function(t, etat, params){
  #Variables d'état
  Pa = etat[1]
  H = etat[2]
  Ea = etat[3]
  Ia = etat[4]
  # healthy root density
  HRD = etat [5]
  
  #Parametres
  r_l = params [1]
  µx_l = params[2]
  lamb_l = params[3]
  alpha_l = params[4]
  bet_l = params[5]
  k_l = params[6]
  et_l = params[7]
  
  #équations du modele
  dPa=-bet_l*Pa*H-et_l*Pa+r_l*Ia
  dH=µx_l*exp(-k_l*((Ia+Ea)/(Ia+Ea+H)))-bet_l*Pa*H
  dEa=bet_l*Pa*H-lamb_l*Ea
  dIa=lamb_l*Ea-alpha_l*Ia
  dHRD=H # calcul du HRD
  
  #Sortie
  return(list(c(dPa,dH,dEa,dIa,dHRD)))
}

# definition du modele pour l'onglet rotation

une_saison_rota<-function(t, etat, params){
  # state variables
  pa = etat[1]
  pv = etat[2]
  H = etat[3]
  Ea = etat[4]
  Ev = etat[5]
  Ia = etat[6]
  Iv = etat[7]
  # healthy root density
  HRD = etat[8]
  
  # parameters
  r_l = params [1]
  mux_l = params[2]
  lamb_l = params[3]
  alpha_l = params[4]
  Ep_a_l = params[5]
  bet_l = params[6]
  k_l = params[7]
  et_l = params[8]
  del_l = params[9]
  wb_l = params[10]
  wr_l = params[11]
  Ep_v_l = params[12]
  
  # equations du modele
  dPa=-bet_l*pa*H-et_l*pa+(1-del_l)*r_l*Ia
  dPv = -bet_l*pv*H-et_l*pv+del_l*r_l*Ia+(1-wr_l)*r_l*Iv
  dH=mux_l*exp(-k_l*((Ia+Iv+Ea+Ev)/(Ia+Iv+Ea+Ev+H)))-Ep_a_l*bet_l*pa*H-(1-wb_l)*bet_l*pv*H*Ep_v_l
  dEa=Ep_a_l*bet_l*pa*H-lamb_l*Ea
  dEv=-lamb_l*Ev+(1-wb_l)*bet_l*pv*H*Ep_v_l
  dIa=lamb_l*Ea-alpha_l*Ia
  dIv=lamb_l*Ev-alpha_l*Iv
  dHRD = H # calcul du HRD 
  
  #sortie pour la fonction ode
  return(list(c(dPa,dPv,dH,dEa,dEv,dIa,dIv,dHRD)))
}


# simulation du modèle matiere organique
# retourne une liste avec un dataframe donnat les différentes simulations et un vecteur de HRD, le long des saisons

integration_MO<-function(init,params,t,n,phi){
  #definition des variables utilisees dans le while
  H0 = init[2]
  
  #decompte des saisons
  current_n<-1 
  
  #Dataframe de sortie
  saisons<-data.frame()
  HRD_saison<-c()
  
  #Application de la fonction ODE dans une boucle saisonnière
  while(current_n<=n){
    #calcul de la première saison et des suivantes avec le while
    int_saison_suivante = ode(init, t, une_saison_MO, params) 
    
    #calcul de la densite à la saison suivante
    dens_hiver_av<-int_saison_suivante[[dim(int_saison_suivante)[1],'1']]*phi # on recupere la taille de int_... via le dim()
    
    # valeurs initiales à la saison suivante 
    init<-c(dens_hiver_av,H0,0,0,0) 
    
    # on sauvegarde les dynamiques de la saison courante
    HRD_saison<-c(HRD_saison,int_saison_suivante[[dim(int_saison_suivante)[1],6]]) #stocke les valeurs de HRD à la fin de chaque saison
    
    int_saison_suivante <- unname(int_saison_suivante) #supprime les noms de colones pour le rbind 
    saisons<-rbind(saisons, int_saison_suivante) #pour pouvoir sortir un tableau avec les resultats de toutes les saisons à la suite  
    
    #passer à la saison suivante
    current_n<-current_n+1 
  }
  
  # nommage du dataframe
  colnames(saisons)<-c("Temps","Nb nematodes","BMR saine", "BMR infectee", "BMR infectieuse ","HRD")
  
  # output
  return(list(tot = saisons, vec_HRD = HRD_saison))
}


# integration du modèle sur plusieurs saisons pour le modèle des rotations culturales
integration_rota<-function(init,params,t,n,phi,P1,P2){
  
  #definition des variables utilisees dans le while
  # notalm 31072024 : pourquoi ca ?
  Ep_a = params[5] 
  H0 = init[3]
  
  #décompte des saisons
  current_n<-1 
  
  # Dataframe prevus en sortie
  saisons<-data.frame()
  HRD_saison<-c()
  
  #Application de la fonction ODE dans une boucle 
  while(current_n<=n){
    #simulation de la première saison et des suivantes avec le while
    int_saison_suivante = ode(init, t, une_saison_rota, params) 
    
    #calcul de la densité en nematodes à la saison suivante
    dens_hiver_av<-int_saison_suivante[[dim(int_saison_suivante)[1],'1']]*phi # on recupere la taille de int_... via le dim()
    dens_hiver_v<-int_saison_suivante[[dim(int_saison_suivante)[1],'2']]*phi
    
    #réinitialisation des valeurs initiales a la saison suivante
    init<-c(dens_hiver_av,dens_hiver_v,H0,0,0,0,0,0) 
    
    # sauvegarde des simulations des saisons successives
    HRD_saison<-c(HRD_saison,int_saison_suivante[[dim(int_saison_suivante)[1],9]])
    
    int_saison_suivante <- unname(int_saison_suivante) #supprime les noms de colones pour le rbind 
    saisons<-rbind(saisons, int_saison_suivante) #pour pouvoir sortir un tableau avec les résultats de toutes les saisons à la suite 
    
    current_n<-current_n+1 #passer à la saison suivante
    
    # notalm 31072024 : pour le coup c'est bizarre que ce ne soit pas en dur
    Ep_a<-rotation_app(n,P1,P2)[current_n] #actualisation de la valeur ep_a
    
    params[5]<-Ep_a #changement de ep_a dans les paramètres
  }
  
  colnames(saisons)<-c("Temps","Nb avirulents","Nb virulents", "BMR saine", 
                       "BMR infectee avir", "BMR infectee vir",
                       "BMR infectieuse avir","BMR infectieuse vir","HRD")
  
  return(list(tot = saisons, vec_HRD = HRD_saison))
}

# reprise pour un tracé plus logique des alternances saisonnieres hiver-ete, en ggplot
# fonction pour tracer les ombrages en hiver
hiverEte <- function(t, lenEte, maxV){
  if(t%%365<lenEte){0}
  else{maxV}
}

# fonction vectorisée
vhiverEte <- Vectorize(hiverEte)
